
#ifeq ($(HPVM_DIR),)
#    $(error HPVM_DIR is undefined! setup_paths.sh needs to be sourced before running make!)
#endif

CC = $(LLVM_BUILD_DIR)/bin/clang
#PLATFORM_CFLAGS = -I$(OPENCL_PATH)/include/CL/ -I$(HPVM_BENCH_DIR)/include
PLATFORM_CFLAGS = -I$(HPVM_BENCH_DIR)/include

CXX = $(LLVM_BUILD_DIR)/bin/clang++
#PLATFORM_CXXFLAGS = -I$(OPENCL_PATH)/include/CL/ -I$(HPVM_BENCH_DIR)/include
PLATFORM_CXXFLAGS = -I$(HPVM_BENCH_DIR)/include

LINKER = $(LLVM_BUILD_DIR)/bin/clang++
#PLATFORM_LDFLAGS = -lm -lpthread -lrt -lOpenCL -L$(OPENCL_LIB_PATH)
PLATFORM_LDFLAGS = -lm -lpthread -lrt -lOpenCL

LLVM_LIB_PATH = $(LLVM_BUILD_DIR)/lib
LLVM_BIN_PATH = $(LLVM_BUILD_DIR)/bin

OPT = $(LLVM_BIN_PATH)/opt
LLVM_LINK = $(LLVM_BIN_PATH)/llvm-link
LLVM_AS = $(LLVM_BIN_PATH)/llvm-as
LIT = $(LLVM_BIN_PATH)/llvm-lit
OCLBE = $(LLVM_BIN_PATH)/llvm-cbe

ifeq ($(TARGET),)
        TARGET = seq
endif

CUR_DIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

# Compiler Flags

LFLAGS += -lm -lrt

# Build dirs
SRC_DIR = src/
ESP_NVDLA_DIR = esp_hardware/nvdla
BUILD_DIR = build/$(TARGET)

INCLUDES +=  -I$(SRC_DIR) -I$(ESP_NVDLA_DIR) -I$(TOP)/core/include

EXE = miniera-hpvm-seq
RISCVEXE = miniera-hpvm-riscv
EPOCHSEXE = miniera-hpvm-epochs

LFLAGS += -pthread

## BEGIN HPVM MAKEFILE
SRCDIR_OBJS=read_trace.ll
OBJS_SRC=$(wildcard $(SRC_DIR)/*.c)
HPVM_OBJS=main.hpvm.ll
APP = miniera-hpvm-$(TARGET)
APP_CFLAGS= -ffast-math $(INCLUDES)
APP_LDFLAGS=$(LFLAGS)

CFLAGS = -O1 $(APP_CFLAGS) $(PLATFORM_CFLAGS)
OBJS_CFLAGS = -O1 $(APP_CFLAGS) $(PLATFORM_CFLAGS)
LDFLAGS= $(APP_LDFLAGS) $(PLATFORM_LDFLAGS)

HPVM_RT_PATH = $(LLVM_BUILD_DIR)/tools/hpvm/projects/hpvm-rt
HPVM_RT_LIB = $(HPVM_RT_PATH)/hpvm-rt.bc

DEVICE = CPU_TARGET
CFLAGS += -DDEVICE=$(DEVICE)
CXXFLAGS += -DDEVICE=$(DEVICE)

# Add BUILDDIR as a prefix to each element of $1
INBUILDDIR=$(addprefix $(BUILD_DIR)/,$(1))

.PRECIOUS: $(BUILD_DIR)/%.ll

OBJS = $(call INBUILDDIR,$(SRCDIR_OBJS))
TEST_OBJS = $(call INBUILDDIR,$(HPVM_OBJS))

KERNEL = $(TEST_OBJS).kernels.ll
HOST = $(BUILD_DIR)/$(APP).host.ll

EPOCHS_HOST = $(BUILD_DIR)/$(APP).host.epochs.ll

HOST_LINKED = $(BUILD_DIR)/$(APP).linked.ll
EPOCHS_LINKED = $(BUILD_DIR)/$(APP).linked.epochs.ll

NVDLA_MODULE = nvdla_runtime
NVDLA_DIR = $(APPROXHPVM_DIR)/llvm/test/VISC/DNN_Benchmarks/benchmarks/miniera-hpvm

LINKER=ld

YEL='\033[0;33m'
NC='\033[0m'

# Targets
.PHONY: default riscv epochs clean
default: $(BUILD_DIR) $(EXE)
riscv: $(BUILD_DIR) $(RISCVEXE)
#epochs: $(FAILSAFE) $(BUILD_DIR) $(EPOCHSEXE)
epochs: check-env $(NVDLA_MODULE) $(FAILSAFE) $(BUILD_DIR) $(EPOCHSEXE) 
#------------------------------------------------------------------------------------------
epochs: CFLAGS += -DENABLE_NVDLA

ROOT = $(CUR_DIR)/sw/umd

NVDLA_RUNTIME_DIR = $(CUR_DIR)/sw/umd/
NVDLA_RUNTIME = $(CUR_DIR)/sw/umd/out

TOOLCHAIN_PREFIX ?= $(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-

ifeq ($(TOOLCHAIN_PREFIX),)
$(error Toolchain prefix missing)
endif

MODULE := nvdla_runtime

include $(ROOT)/make/macros.mk

BUILDOUT ?= $(ROOT)/out/apps/runtime
BUILDDIR := $(BUILDOUT)/$(MODULE)
TEST_BIN := $(BUILDDIR)/$(MODULE)

MODULE_COMPILEFLAGS := -W -Wall -Wno-multichar -Wno-unused-parameter -Wno-unused-function -Werror-implicit-function-declaration
MODULE_CFLAGS := --std=c99
MODULE_CPPFLAGS := --std=c++11 -fexceptions -fno-rtti
NVDLA_FLAGS := -pthread -L$(ROOT)/external/ -ljpeg -L$(ROOT)/out/core/src/runtime/libnvdla_runtime -lnvdla_runtime  -Wl,-rpath=.

include esp_hardware/nvdla/rules.mk
#-----------------------------------------------------------------------------------------------


check-env:
#ifndef APPROXHPVM_DIR
#	$(error APPROXHPVM_DIR is undefined! setup_paths.sh needs to be sourced before running make!)
#endif
#ifndef MINIERA_DIR
#	$(error MINIERA_DIR is undefined! setup_paths.sh needs to be sourced before running make!)
#endif
ifndef RISCV_BIN_DIR
	$(error RISCV_BIN_DIR is undefined! setup_paths.sh needs to be sourced before running make!)
endif

$(NVDLA_MODULE):
	@echo -e ${YEL}Compiling NVDLA Runtime Library${NC}
	@cd $(NVDLA_RUNTIME_DIR) && make runtime

	#---------------
	#$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/core/include/ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/apps/runtime/  -I./sw/umd/external/include/      -c  ./sw/umd/apps/runtime/RuntimeTest.cpp   -o   runtime_test.o
	#$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/core/include/ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/apps/runtime/  -I./sw/umd/external/include/      -c  ./sw/umd/apps/runtime/TestUtils.cpp   -o   TestUtils.o
	#$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/core/include/ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/apps/runtime/  -I./sw/umd/external/include/      -c  ./sw/umd/apps/runtime/DlaImageUtils.cpp   -o   DlaImageUtils.o
	#$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/core/include/ -I/home/hsharif3/Github/aejjeh/mini-era/sw/umd/apps/runtime/  -I./sw/umd/external/include/      -c  ./sw/umd/apps/runtime/DlaImage.cpp   -o   DlaImage.o
	#$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++   runtime_test.o TestUtils.o DlaImageUtils.o  DlaImage.o   -o  runtime_test    -mabi=lp64d -march=rv64g	$(NVDLA_FLAGS)

	#--------------------


	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I./sw/umd/core/include/ -I./esp_hardware/nvdla/  -I./sw/umd/external/include/      -c  ./esp_hardware/nvdla/nvdla_main.cpp   -o   nvdla_main.o
	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I./sw/umd/core/include/ -I./esp_hardware/nvdla/  -I./sw/umd/external/include/      -c  ./esp_hardware/nvdla/RuntimeTest.cpp   -o   runtime_test.o
	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I./sw/umd/core/include/ -I./esp_hardware/nvdla/  -I./sw/umd/external/include/      -c  ./esp_hardware/nvdla/TestUtils.cpp   -o   TestUtils.o
	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I./sw/umd/core/include/ -I./esp_hardware/nvdla/  -I./sw/umd/external/include/      -c  ./esp_hardware/nvdla/DlaImageUtils.cpp   -o   DlaImageUtils.o
	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I./sw/umd/core/include/ -I./esp_hardware/nvdla/  -I./sw/umd/external/include/      -c  ./esp_hardware/nvdla/DlaImage.cpp   -o   DlaImage.o
	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++ -I./sw/umd/core/include/ -I./esp_hardware/nvdla/  -I./sw/umd/external/include/      -c  ./esp_hardware/nvdla/Server.cpp   -o   Server.o
	$(RISCV_BIN_DIR)/riscv64-unknown-linux-gnu-g++   nvdla_main.o runtime_test.o TestUtils.o DlaImageUtils.o  DlaImage.o  Server.o  -o  esp_test    -mabi=lp64d -march=rv64g	$(NVDLA_FLAGS)


