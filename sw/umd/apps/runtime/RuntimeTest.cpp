/*
 * Copyright (c) 2017-2019, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "DlaImageUtils.h"
//#include "ErrorMacros.h"
#include "RuntimeTest.h"

#include <time.h>
#include <sys/time.h>
#include <stdio.h>

#include "nvdla/IRuntime.h"

#include "half.h"
#include "main.h"
#include "nvdla_os_inf.h"

#include "dlaerror.h"
#include "dlatypes.h"

#include <cstdio> // snprintf, fopen
#include <string>

#define OUTPUT_DIMG "output.dimg"

using namespace half_float;

static TestImageTypes getImageType(std::string imageFileName)
{
    TestImageTypes it = IMAGE_TYPE_UNKNOWN;
    std::string ext = imageFileName.substr(imageFileName.find_last_of(".") + 1);
    if (ext == "pgm")
    {
        it = IMAGE_TYPE_PGM;
    }
    else if (ext == "jpg")
    {
        it = IMAGE_TYPE_JPG;
    }

    return it;
}

static NvDlaError copyImageToInputTensor
(
    const TestAppArgs* appArgs,
    TestInfo* i,
    void** pImgBuffer,
    nvdla::IRuntime::NvDlaTensor *tensorDesc
)
{
    NvDlaError e = NvDlaSuccess;

    std::string imgPath = /*i->inputImagesPath + */appArgs->inputName;

    printf("WATCH OUT ***** inputName = %s \n", appArgs->inputName);
    
    NvDlaImage* R8Image = new NvDlaImage();
    NvDlaImage* tensorImage = NULL;
    TestImageTypes imageType = getImageType(imgPath);
    if (!R8Image){
      //ORIGINATE_ERROR(NvDlaError_InsufficientMemory);
    }

    switch(imageType) {
        case IMAGE_TYPE_PGM:
	  PGM2DIMG(imgPath, R8Image, tensorDesc);
            break;
        case IMAGE_TYPE_JPG:
	  JPEG2DIMG(imgPath, R8Image, tensorDesc);
            break;
        default:
            //TODO Fix this error condition
//          ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "Unknown image type: %s", imgPath.c_str());
            printf("Unknown image type: %s", imgPath.c_str());
            goto fail;
    }

    tensorImage = i->inputImage;
    if (tensorImage == NULL){
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "NULL input Image");
      printf("  NULL input image \n");
    }
	
    createImageCopy(appArgs, R8Image, tensorDesc, tensorImage);

    //tensorImage->printBuffer(true);  /* Print the input Buffer */ 

    DIMG2DlaBuffer(tensorImage, pImgBuffer);

fail:
    if (R8Image != NULL && R8Image->m_pData != NULL)
        NvDlaFree(R8Image->m_pData);
    delete R8Image;

    return e;
}

static NvDlaError prepareOutputTensor
(
    nvdla::IRuntime::NvDlaTensor* pTDesc,
    NvDlaImage* pOutImage,
    void** pOutBuffer,
    const TestAppArgs* appArgs
)
{
    NvDlaError e = NvDlaSuccess;

    Tensor2DIMG(appArgs, pTDesc, pOutImage);
    DIMG2DlaBuffer(pOutImage, pOutBuffer);

fail:
    return e;
}


NvDlaError setupInputBuffer
(
    const TestAppArgs* appArgs,
    TestInfo* i,
    void** pInputBuffer
)
{
    NvDlaError e = NvDlaSuccess;
    void *hMem = NULL;
    NvS32 numInputTensors = 0;
    nvdla::IRuntime::NvDlaTensor tDesc;

    nvdla::IRuntime* runtime = i->runtime;
    if (!runtime){
      // ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "getRuntime() failed");
      printf(" getRuntime() failed \n");
    }

    runtime->getNumInputTensors(&numInputTensors);

    i->numInputs = numInputTensors;

    if (numInputTensors < 1)
        goto fail;

    runtime->getInputTensorDesc(0, &tDesc);

    runtime->allocateSystemMemory(&hMem, tDesc.bufferSize, pInputBuffer);
    i->inputHandle = (NvU8 *)hMem;
    copyImageToInputTensor(appArgs, i, pInputBuffer, &tDesc);

    if (!runtime->bindInputTensor(0, hMem)){
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "runtime->bindInputTensor() failed");
      printf("runtime->bindiInputTensor() failed \n");
    }

fail:
    return e;
}

static void cleanupInputBuffer(const TestAppArgs *appArgs,
                                TestInfo *i)
{
    nvdla::IRuntime *runtime = NULL;
    NvS32 numInputTensors = 0;
    nvdla::IRuntime::NvDlaTensor tDesc;
    NvDlaError e = NvDlaSuccess;

    if (i->inputImage != NULL && i->inputImage->m_pData != NULL) {
        NvDlaFree(i->inputImage->m_pData);
        i->inputImage->m_pData = NULL;
    }

    runtime = i->runtime;
    if (runtime == NULL)
        return;
    e = runtime->getNumInputTensors(&numInputTensors);
    if (e != NvDlaSuccess)
        return;

    if (numInputTensors < 1)
        return;

    e = runtime->getInputTensorDesc(0, &tDesc);
    if (e != NvDlaSuccess)
        return;

    if (i->inputHandle == NULL)
        return;

    /* Free the buffer allocated */
    runtime->freeSystemMemory(i->inputHandle, tDesc.bufferSize);
    i->inputHandle = NULL;
    return;
}

NvDlaError setupOutputBuffer
(
    const TestAppArgs* appArgs,
    TestInfo* i,
    void** pOutputBuffer
)
{
  //NVDLA_UNUSED(appArgs);

    NvDlaError e = NvDlaSuccess;
    void *hMem;
    NvS32 numOutputTensors = 0;
    nvdla::IRuntime::NvDlaTensor tDesc;
    NvDlaImage *pOutputImage = NULL;

    nvdla::IRuntime* runtime = i->runtime;
    if (!runtime)
      printf(" getRuntime() failed \n");
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "getRuntime() failed");

    runtime->getNumOutputTensors(&numOutputTensors);

    i->numOutputs = numOutputTensors;

    if (numOutputTensors < 1){
      printf("expected num of output tensors issue  \n");
    }
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "Expected number of output tensors of %u, found %u", 1, numOutputTensors);

    runtime->getOutputTensorDesc(0, &tDesc);
    runtime->allocateSystemMemory(&hMem, tDesc.bufferSize, pOutputBuffer);
    i->outputHandle = (NvU8 *)hMem;

    pOutputImage = i->outputImage;
    if (i->outputImage == NULL){
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "NULL Output image");
      printf(" NULL output image \n");
    }
    
    prepareOutputTensor(&tDesc, pOutputImage, pOutputBuffer, appArgs);

    if (!runtime->bindOutputTensor(0, hMem)){
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "runtime->bindOutputTensor() failed");
      printf("runtime->bindOutputTensor failed \n");
    }

fail:
    return e;
}

static void cleanupOutputBuffer(const TestAppArgs *appArgs,
                                TestInfo *i)
{
    nvdla::IRuntime *runtime = NULL;
    NvS32 numOutputTensors = 0;
    nvdla::IRuntime::NvDlaTensor tDesc;
    NvDlaError e = NvDlaSuccess;

    /* Do not clear outputImage if in server mode */
    if (!i->dlaServerRunning &&
            i->outputImage != NULL &&
            i->outputImage->m_pData != NULL) {
        NvDlaFree(i->outputImage->m_pData);
        i->outputImage->m_pData = NULL;
    }

    runtime = i->runtime;
    if (runtime == NULL)
        return;
    e = runtime->getNumOutputTensors(&numOutputTensors);
    if (e != NvDlaSuccess)
        return;
    e = runtime->getOutputTensorDesc(0, &tDesc);
    if (e != NvDlaSuccess)
        return;

    if (i->outputHandle == NULL)
        return;

    /* Free the buffer allocated */
    runtime->freeSystemMemory(i->outputHandle, tDesc.bufferSize);
    i->outputHandle = NULL;
    return;
}

static NvDlaError readLoadable(const TestAppArgs* appArgs, TestInfo* i)
{
    NvDlaError e = NvDlaSuccess;
    //NVDLA_UNUSED(appArgs);
    std::string loadableName;
    NvDlaFileHandle file;
    NvDlaStatType finfo;
    size_t file_size;
    NvU8 *buf = 0;
    size_t actually_read = 0;
    NvDlaError rc;

    // Determine loadable path
    if (appArgs->loadableName == "")
    {
      //ORIGINATE_ERROR_FAIL(NvDlaError_NotInitialized, "No loadable found to load");
    }

    loadableName = appArgs->loadableName;

    rc = NvDlaFopen(loadableName.c_str(), NVDLA_OPEN_READ, &file);
    if (rc != NvDlaSuccess)
    {
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "couldn't open %s\n", loadableName.c_str());
    }

    rc = NvDlaFstat(file, &finfo);
    if ( rc != NvDlaSuccess)
    {
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "couldn't get file stats for %s\n", loadableName.c_str());
    }

    file_size = NvDlaStatGetSize(&finfo);
    if ( !file_size ) {
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "zero-length for %s\n", loadableName.c_str());
    }

    buf = new NvU8[file_size];

    NvDlaFseek(file, 0, NvDlaSeek_Set);

    rc = NvDlaFread(file, buf, file_size, &actually_read);
    if ( rc != NvDlaSuccess )
    {
        NvDlaFree(buf);
        //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "read error for %s\n", loadableName.c_str());
    }
    
    NvDlaFclose(file);
    if ( actually_read != file_size ) {
        NvDlaFree(buf);
        //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "read wrong size for buffer? %d\n", actually_read);
    }

    i->pData = buf;

fail:
    return e;
}

NvDlaError loadLoadable(const TestAppArgs* appArgs, TestInfo* i)
{
    NvDlaError e = NvDlaSuccess;

    nvdla::IRuntime* runtime = i->runtime;
    if (!runtime){
      //  ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "getRuntime() failed");
    }

    if (!runtime->load(i->pData, 0)){
      //  ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "runtime->load failed");
    }

fail:
    return e;
}

void unloadLoadable(const TestAppArgs* appArgs, TestInfo *i)
{
  //NVDLA_UNUSED(appArgs);
    nvdla::IRuntime *runtime = NULL;

    runtime = i->runtime;
    if (runtime != NULL) {
        runtime->unload();
    }
}

double get_elapsed_time(struct timespec *before, struct timespec *after)
{
  double deltat_s  = (after->tv_sec - before->tv_sec) * 1000000;
  double deltat_ns = (after->tv_nsec - before->tv_nsec) / 1000;
  return deltat_s + deltat_ns;
}


  
NvDlaError runTest(const TestAppArgs* appArgs, TestInfo* i)
{
    NvDlaError e = NvDlaSuccess;
    void* pInputBuffer = NULL;
    void* pOutputBuffer = NULL;
    struct timespec before, after;

    nvdla::IRuntime* runtime = i->runtime;
    if (!runtime)
      printf ("  getRuntime() failed \n ");
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "getRuntime() failed");

    i->inputImage = new NvDlaImage();
    i->outputImage = new NvDlaImage();

    //PROPAGATE_ERROR_FAIL(setupInputBuffer(appArgs, i, &pInputBuffer));
    setupInputBuffer(appArgs, i, &pInputBuffer);
    
    //PROPAGATE_ERROR_FAIL(setupOutputBuffer(appArgs, i, &pOutputBuffer));
    setupOutputBuffer(appArgs, i, &pOutputBuffer);
    
    //NvDlaDebugPrintf("submitting tasks...\n");
    printf("submiting tasks... \n");
    
    clock_gettime(CLOCK_MONOTONIC, &before);
    if (!runtime->submit())
      printf (" runtime->submit() failed \n");
      //ORIGINATE_ERROR(NvDlaError_BadParameter, "runtime->submit() failed");

    clock_gettime(CLOCK_MONOTONIC, &after);
    //NvDlaDebugPrintf("execution time = %f s\n", get_elapsed_time(&before,&after));

    printf("execution time = %f s\n", get_elapsed_time(&before,&after));
    
    DlaBuffer2DIMG(&pOutputBuffer, i->outputImage);

    //i->outputImage->printBuffer(true);   /* Print the output buffer */

    // HASH_NOTE ::::::: 
    /* Dump output dimg to a file */
    DIMG2DIMGFile(i->outputImage,
		  OUTPUT_DIMG,
		  true,
		  appArgs->rawOutputDump);

fail:
    cleanupOutputBuffer(appArgs, i);
    /* Do not clear outputImage if in server mode */
    if (!i->dlaServerRunning && i->outputImage != NULL) {
        delete i->outputImage;
        i->outputImage = NULL;
    }

    cleanupInputBuffer(appArgs, i);
    if (i->inputImage != NULL) {
        delete i->inputImage;
        i->inputImage = NULL;
    }

    return e;
}


NvDlaError run(const TestAppArgs* appArgs, TestInfo* i)
{
    NvDlaError e = NvDlaSuccess;

    /* Create runtime instance */
    printf("Creating new runtime context \n");
    
    i->runtime = nvdla::createRuntime();
    if (i->runtime == NULL)
      printf ("createRuntime() failed \n");
      //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "createRuntime() failed");

    printf("calling read loadable \n");
    if (!i->dlaServerRunning)
        readLoadable(appArgs, i);

    /* Load loadable */
    loadLoadable(appArgs, i);

    printf("run Loadable passed \n");

    /* Start emulator */
    if (!i->runtime->initEMU())
      printf(" runtime->initEMU() failed \n");


//ORIGINATE_ERROR(NvDlaError_DeviceNotFound, "runtime->initEMU() failed");

/****
    
   runTest(appArgs, i);

fail:
 
   // Stop emulator 
    if (i->runtime != NULL)
        i->runtime->stopEMU();

    // Unload loadables 
    unloadLoadable(appArgs, i);

    // Free if allocated in read Loadable 
    if (!i->dlaServerRunning && i->pData != NULL) {
        delete[] i->pData;
        i->pData = NULL;
    }

    // Destroy runtime 

    nvdla::destroyRuntime(i->runtime);
    return e;

*/

   return e;
}


// NOTE: code borrowed from nvdla_main.cpp

TestAppArgs defaultTestAppArgs = TestAppArgs();
//TestAppArgs defaultTestAppArgs = TestAppArgs();

TestAppArgs testAppArgs = defaultTestAppArgs;
TestInfo testInfo;


NvDlaError testSetup(const TestAppArgs* appArgs, TestInfo* i)
//NvDlaError testSetup(const TestAppArgs* appArgs, TestInfo* i)
{
    NvDlaError e = NvDlaSuccess;

    //std::string imagePath("seven.pgm");

    std::string imagePath  = "";
    std::string imagePath2 = imagePath;
    //appArgs->inputName = imagePath;  //"seven.pgm"; //  imagePath;
    NvDlaStatType stat;

    // Do input paths exist?
    if (std::strcmp(appArgs->inputName.c_str(), "") != 0)
    {
        e = NvDlaStat(appArgs->inputPath.c_str(), &stat);
        if (e != NvDlaSuccess)
	  printf("Input path does not exist: \"%s\"", appArgs->inputPath.c_str());
	  //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "Input path does not exist: \"%s\"", appArgs->inputPath.c_str());

        imagePath = /* appArgs->inputPath + "/images/" + */appArgs->inputName;
        e = NvDlaStat(imagePath.c_str(), &stat);
        if (e != NvDlaSuccess)
	  printf("Image path does not exist: \"%s/%s\"", imagePath.c_str());
	  //ORIGINATE_ERROR_FAIL(NvDlaError_BadParameter, "Image path does not exist: \"%s/%s\"", imagePath.c_str());
    }

    return NvDlaSuccess;

fail:
    return e;
}

/*static NvDlaError launchServer(const TestAppArgs* appArgs)
//NvDlaError launchServer(const TestAppArgs* appArgs)
{
    NvDlaError e = NvDlaSuccess;
    TestInfo testInfo;

    testInfo.dlaServerRunning = false;
    runServer(appArgs, &testInfo);

fail:
    return e;
}
*/

NvDlaError launchTest(const TestAppArgs* appArgs)
//NvDlaError launchTest(const TestAppArgs* appArgs)
{
    NvDlaError e = NvDlaSuccess;

    testInfo.dlaServerRunning = false;
    //TestInfo testInfo;
    testSetup(appArgs, &testInfo);
    //PROPAGATE_ERROR_FAIL(testSetup(testAppArgs, &testInfo));

    run(appArgs, &testInfo);
    //PROPAGATE_ERROR_FAIL(run(testAppArgs, &testInfo));

fail:
    return e;
}



// HASH-NOTE::: 
void runImageonNVDLA(std::string iImage) {
    NvDlaError e = NvDlaError_TestApplicationFailed;
    testAppArgs.inputName = iImage;
    testAppArgs.rawOutputDump = true;
    //runImage(&testAppArgs, &testInfo));
    //e = runImage(&testAppArgs, &testInfo);

    e = runTest(&testAppArgs, &testInfo);
    
    if (e != NvDlaSuccess)
    {
        //return EXIT_FAILURE;
        NvDlaDebugPrintf("Test failed\n");
    }
    else
    {
        NvDlaDebugPrintf("Image Processed, Test Passed\n");
    }

}


// HASH-NOTE::: separation of loading module and running tests
// This function reads the loadable and creates a new runtime context
void initNVDLA(std::string module_name) {

    NvDlaError e = NvDlaError_TestApplicationFailed;
//    TestAppArgs testAppArgs = defaultTestAppArgs;
    bool serverMode = false;
    bool inputPathSet = false;

    //NVDLA_UNUSED(inputPathSet);
    //testAppArgs.loadableName = "mnist_loadable2.nvdla";
    //testAppArgs.loadableName = "hpvm-mod.nvdla";

    testAppArgs.loadableName = module_name;

    // Launch
    e = launchTest(&testAppArgs);

    if (e != NvDlaSuccess)
    {
        //return EXIT_FAILURE;
        NvDlaDebugPrintf("Runtime creation failed and loadable not read\n");
    }
    else
    {
        NvDlaDebugPrintf("Loadable read and Runtime creation successful\n");
    }

}





int main(){

  //initNVDLA("lenet_mnist.nvdla");

  initNVDLA("miniera.nvdla");

  runImageonNVDLA("./images_era/0094_0.jpg");
  runImageonNVDLA("./images_era/0094_0.jpg");
  runImageonNVDLA("./images_era/0094_0.jpg");
  runImageonNVDLA("./images_era/0094_0.jpg");
  
  //runImageonNVDLA("seven.pgm");
  //runImageonNVDLA("seven.pgm");
  //runImageonNVDLA("seven.pgm");
  //runImageonNVDLA("seven.pgm");
  //runImageonNVDLA("seven.pgm");
  // runImageonNVDLA("seven.pgm");
  //runImageonNVDLA("seven.pgm");
  //runImageonNVDLA("seven.pgm");

  
  return 0;
}


